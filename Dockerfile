FROM node:9

USER node
WORKDIR /home/node

ENV PATH=/home/node/.yarn/bin:$PATH

RUN yarn global add gulp